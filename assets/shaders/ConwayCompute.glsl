#version 460 core

layout(local_size_x = 16, local_size_y = 8, local_size_z = 1) in;

layout(rgba8, binding = 0) uniform restrict readonly image2D in_screen;
layout(rgba8, binding = 1) uniform restrict writeonly image2D out_screen;

const vec4 DEAD = vec4(0, 0, 0, 1);
const vec4 ALIVE = vec4(1, 1, 1, 1);

int get_pixel_value(ivec2 pixel_coords, ivec2 image_size)
{
    return int(imageLoad(in_screen, pixel_coords).r == 1);
}

void main()
{
    ivec2 pixel_coords = ivec2(gl_GlobalInvocationID.xy);
    ivec2 image_size = imageSize(in_screen);

    float g = float(pixel_coords.x) / float(max(1, image_size.x));
    float b = float(pixel_coords.y) / float(max(1, image_size.y));

    vec4 current = imageLoad(in_screen, pixel_coords);

    int neighbours = 0;
    for(int y = 0; y < 3; ++y)
    {
        for(int x = 0; x < 3; ++x)
        {
            if(x == 1 && y == 1)
            {
                continue;
            }
            neighbours += get_pixel_value(pixel_coords + ivec2(x - 1, y - 1), image_size);
        }
    }

    switch(neighbours)
    {
        case 2:
            imageStore(out_screen, pixel_coords, current);
            break;
        case 3:
            imageStore(out_screen, pixel_coords, vec4(1, g, b, 1));
            break;
        default:
            imageStore(out_screen, pixel_coords, DEAD);
            break;
    }
}
