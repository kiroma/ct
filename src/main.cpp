#include <cstdlib>
#include <numbers>

#include <SFML/Graphics/Image.hpp>
#include <SFML/Window/Event.hpp>
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Graphics/GLDebugEnable.h"
#include "Graphics/Shader.h"
#include "Graphics/Texture.h"
#include "Graphics/VertexArray.h"
#include "Util.h"

int main()
{
    sf::ContextSettings context_settings;
    context_settings.depthBits = 0;
    context_settings.stencilBits = 0;
    context_settings.antialiasingLevel = 0;
    context_settings.majorVersion = 4;
    context_settings.minorVersion = 6;
    context_settings.attributeFlags = sf::ContextSettings::Core
#ifdef NDEBUG
        | GL_CONTEXT_FLAG_NO_ERROR_BIT
#else
        | GL_CONTEXT_FLAG_DEBUG_BIT
#endif
        ;

    sf::Window window({2048, 2048}, "Compute Conway Game of Life", sf::Style::Default, context_settings);
    window.setVerticalSyncEnabled(true);
    bool mouse_locked = false;

    window.setActive(true);

    if (!gladLoadGL())
    {
        std::cerr << "Failed to init OpenGL - Is OpenGL linked correctly?\n";
        return -1;
    }
    glViewport(0, 0, 2048, 2048);
    glScissor(0, 0, 2048, 2048);
    init_opengl_debugging();
    // GUI::init(&window);

    mus::VertexArray screen_vao;

    mus::Shader screen_shader;
    if (!screen_shader.load_stage("assets/shaders/ScreenVertex.glsl", mus::ShaderType::Vertex) ||
        !screen_shader.load_stage("assets/shaders/ScreenFragment.glsl", mus::ShaderType::Fragment) ||
        !screen_shader.link_shaders())
    {
        return -1;
    }

    mus::Shader compute_shader;
    if (!compute_shader.load_stage("assets/shaders/ConwayCompute.glsl", mus::ShaderType::Compute) ||
        !compute_shader.link_shaders())
    {
        return -1;
    }

    sf::Image image;
    image.create(2048, 2048);
    for (unsigned y = 0; y < image.getSize().y; y++)
    {
        for (unsigned x = 0; x < image.getSize().x; x++)
        {
            image.setPixel(x, y, rand() % 50 > 20 ? sf::Color::Black : sf::Color::White);
        }
    }
    mus::Texture2D screen_texture;
    mus::Texture2D screen_texture2;
    screen_texture.load_from_image(image, 1, mus::TextureInternalFormat::RGBA, mus::TextureFormat::RGBA8);
    // screen_texture.create(window.getSize().x, window.getSize().y, 1,
    //                       mus::TextureFormat::RGBA32F);
    screen_texture.set_wrap_s(mus::TextureWrap::ClampToEdge);
    screen_texture.set_wrap_t(mus::TextureWrap::ClampToEdge);
    screen_texture.set_min_filter(mus::TextureMinFilter::Nearest);
    screen_texture.set_mag_filter(mus::TextureMagFilter::Nearest);

    screen_texture2.create(window.getSize().x, window.getSize().y, 1, mus::TextureFormat::RGBA8);
    screen_texture2.set_wrap_s(mus::TextureWrap::ClampToEdge);
    screen_texture2.set_wrap_t(mus::TextureWrap::ClampToEdge);
    screen_texture2.set_min_filter(mus::TextureMinFilter::Nearest);
    screen_texture2.set_mag_filter(mus::TextureMagFilter::Nearest);

    GLuint fboId = 0;
    glGenFramebuffers(1, &fboId);
    glBindFramebuffer(GL_READ_FRAMEBUFFER, fboId);

    bool gen = false;

    // This is needed for the compute shader
    // glBindImageTexture(0, screen_texture.id, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);

    sf::Clock timer;

    // -------------------
    // ==== Main Loop ====
    // -------------------
    window.setFramerateLimit(0);
    window.setVerticalSyncEnabled(false);
    glViewport(0, 0, window.getSize().x, window.getSize().y);

    compute_shader.bind();
    while (window.isOpen())
    {

        // GUI::begin_frame();
        sf::Event e;
        while (window.pollEvent(e))
        {
            // GUI::event(window, e);
            if (e.type == sf::Event::Closed)
                window.close();
            else if (e.type == sf::Event::KeyReleased)
            {
                if (e.key.code == sf::Keyboard::Escape)
                {
                    window.close();
                }
                else if (e.key.code == sf::Keyboard::L)
                {
                    mouse_locked = !mouse_locked;
                }
            }
        }
        if (!window.isOpen())
        {
            break;
        }

        // window.setMouseCursorVisible(mouse_locked);

        // glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        if (!gen)
        {
            glBindImageTexture(0, screen_texture.id, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
            glBindImageTexture(1, screen_texture2.id, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
            screen_texture2.bind(0);
            glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, screen_texture2.id, 0);
        }
        else
        {
            glBindImageTexture(0, screen_texture2.id, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA8);
            glBindImageTexture(1, screen_texture.id, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
            screen_texture.bind(0);
            glFramebufferTexture2D(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, screen_texture.id, 0);
        }
        gen = !gen;

        glDispatchCompute(2048 / 16, 2048 / 8, 1);
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        // screen_shader.bind();
        // screen_vao.bind();
        // glDrawArrays(GL_TRIANGLES, 0, 3);
        glBlitFramebuffer(0, 0, 2048, 2048, 0, 0, 2048, 2048, GL_COLOR_BUFFER_BIT, GL_NEAREST);

        // GUI::render();
        window.display();
    }

    // --------------------------
    // ==== Graceful Cleanup ====
    // --------------------------
    // GUI::shutdown();
}
